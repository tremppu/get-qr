import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import * as qrcode from 'qrcode';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements AfterViewInit, OnDestroy {
  text: string;
  imageFileSVG: string | SafeUrl;
  imageFileJPG: string | SafeUrl;
  imageFilePNG: string | SafeUrl;
  subs: Subscription;
  constructor(private sanitizer: DomSanitizer) {}

  ngAfterViewInit() {
    this.updateQr();
    this.subs = fromEvent(document, 'keyup')
      .pipe(debounceTime(500))
      .subscribe(() => this.updateQr());
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  async updateQr() {
    this.imageFileSVG = await qrcode
      .toString(this.text || 'Teemukissa Teemukissa', {
        type: 'svg',
        width: 1000
      })
      .then(res => {
        const blob = new Blob([res], { type: 'image/svg+xml' });
        const url = URL.createObjectURL(blob);
        return this.sanitizer.bypassSecurityTrustUrl(url);
      });

    this.imageFileJPG = await qrcode
      .toDataURL(null, this.text, { type: 'image/jpeg', width: 1000 })
      .then(res => this.sanitizer.bypassSecurityTrustUrl(res))
      .catch(() => null);

    this.imageFilePNG = await qrcode
      .toDataURL(null, this.text, { type: 'image/png', width: 1000 })
      .then(res => this.sanitizer.bypassSecurityTrustUrl(res))
      .catch(() => null);
  }

  scrollToBottom() {
    setTimeout(() => {
      window.scrollTo(0, document.body.scrollHeight);
    }, 200);
  }
}
