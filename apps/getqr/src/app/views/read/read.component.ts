import {
  Component,
  AfterViewInit,
  ViewChild,
  ElementRef,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import jsQR from 'jsQR';
import { MatSnackBar } from '@angular/material/snack-bar';
import { distinctUntilChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadComponent implements AfterViewInit, OnDestroy {
  result = '';

  read = new EventEmitter<string>();

  isVcard: boolean;

  @ViewChild('video')
  video: ElementRef<HTMLVideoElement>;
  @ViewChild('canvas')
  canvas: ElementRef<HTMLCanvasElement>;

  subs: Subscription;

  stream: MediaStream;

  constructor(private snackbar: MatSnackBar) {}

  async ngAfterViewInit() {
    const video = this.video.nativeElement;
    const stream = await navigator.mediaDevices.getUserMedia({
      video: { facingMode: 'environment' }
    });

    video.srcObject = stream;
    video.setAttribute('playsinline', 'true'); // required to tell iOS safari we don't want fullscreen
    video.play();

    requestAnimationFrame(this.tick.bind(this));

    this.subs = this.read.pipe(distinctUntilChanged()).subscribe(() => {
      navigator.vibrate(500);
      this.snackbar.open('QR code read 🎉', null, {
        duration: 2000,
        verticalPosition: 'top'
      });
    });

    this.stream = stream;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    if (this.stream && this.stream.active) {
      this.stream.getVideoTracks()[0].stop();
    }
  }

  tick() {
    const video = this.video.nativeElement;
    const canvas = this.canvas.nativeElement;
    if (video.readyState === video.HAVE_ENOUGH_DATA) {
      const ctx = canvas.getContext('2d');
      ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
      const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
      const code = jsQR(imageData.data, imageData.width, imageData.height, {
        inversionAttempts: 'dontInvert'
      });
      if (code) {
        this.result = code.data;
        this.read.emit(code.data);
        if (code.data.startsWith('BEGIN:VCARD')) {
          // this.isVcard = true;
        } else {
          this.isVcard = false;
        }
      }
    }
    requestAnimationFrame(this.tick.bind(this));
  }

  copy() {
    const _navigator = navigator as any;
    if (!this.result) {
      return this.snackbar.open('Nothing to copy!', null, {
        duration: 2000,
        verticalPosition: 'top'
      });
    }
    _navigator.clipboard
      .writeText(this.result)
      .then(() => {
        this.snackbar.open('Text copied to clipboard 🎉', null, {
          duration: 2000,
          verticalPosition: 'top'
        });
      })
      .catch(err => {
        console.error('Could not copy text: ', err);
        this.snackbar.open('Could not copy text :(', null, {
          duration: 2200,
          panelClass: 'warn',
          verticalPosition: 'top'
        });
      });
  }
}
